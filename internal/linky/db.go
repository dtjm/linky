package linky

import (
	"database/sql"
)

// db returns a new database connection.  the Sqlite file should be reopened on every request to ensure the latest file is being used
func (a *App) db() (*sql.DB, error) {
	return sql.Open("sqlite3", a.Cfg.DBPath)
}
