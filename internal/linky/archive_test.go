package linky_test

import (
	"bytes"
	"compress/zlib"
	"io"
	"testing"
	"testing/quick"
)

func TestZlib(t *testing.T) {

	f := func(data []byte) bool {
		out := &bytes.Buffer{}
		zw, err := zlib.NewWriterLevel(out, zlib.BestCompression)
		if err != nil {
			return false
		}

		_, err = zw.Write(data)
		if err != nil {
			return false
		}
		err = zw.Close()
		if err != nil {
			return false
		}

		zr, err := zlib.NewReader(out)
		if err != nil {
			return false
		}

		uncompressed := &bytes.Buffer{}
		_, err = io.Copy(uncompressed, zr)
		if err != nil {
			t.Log(err)
			return false
		}

		if !bytes.Equal(data, uncompressed.Bytes()) {
			t.Logf("uncompressed != input")
			return false
		}

		return true
	}

	err := quick.Check(f, nil)
	if err != nil {
		t.Fatal(err)
	}
}
