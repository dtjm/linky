package linky

func (a *App) addToSearchIndex(b *Bookmark) {
	content, err := b.plaintext()
	if err != nil {
		a.Cfg.Logger.Printf("addToSearchIndex: error getting plaintext: %s", err)
		return
	}

	db, err := a.db()
	if err != nil {
		a.Cfg.Logger.Printf("addToSearchIndex: error opening database: %s", err)
		return
	}
	defer db.Close()

	_, err = db.Exec(`
	INSERT INTO search(rowid, url, title, description, tags, archive_data)
	VALUES 			  (    ?,   ?,     ?,           ?,    ?,            ?)
	`, b.RowID, b.URL, b.Title, b.Description, b.Tags, content)
	if err != nil {
		a.Cfg.Logger.Printf("error adding to search index: %s", err)
		return
	}

	excerpt := content
	if len(excerpt) > 50 {
		excerpt = excerpt[:50]
	}
	a.Cfg.Logger.Printf("addToSearchIndex(%d): %s", b.RowID, excerpt)
}
