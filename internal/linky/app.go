package linky

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/alecthomas/template"
	"github.com/pkg/errors"
	"golang.org/x/net/html"
)

type App struct {
	Cfg struct {
		Logger *log.Logger

		ListenAddr string
		DBPath     string

		ImportNetscapePath string
	}

	deps struct {
		server    *http.Server
		templates *template.Template
	}

	state struct {
		// ensures that only N archivers are running
		archiverSema int64
	}
}

func (a *App) Start() error {
	db, err := a.db()
	if err != nil {
		return errors.WithMessage(err, fmt.Sprintf("error opening sqlite database %s", a.Cfg.DBPath))
	}
	a.Cfg.Logger.Printf("opened database at %s", a.Cfg.DBPath)

	res, err := db.Exec(`
	CREATE TABLE IF NOT EXISTS
	bookmarks (
		url 		TEXT UNIQUE NOT NULL DEFAULT "",
		title 		TEXT NOT NULL DEFAULT "",
		description TEXT NOT NULL DEFAULT "",
		tags 		TEXT NOT NULL DEFAULT "",

		-- blob of archive content
		archive_data BLOB NOT NULL DEFAULT "",

		-- HTTP status of GET request when archiving
		archive_status INT NOT NULL DEFAULT 0,

		-- 0 or 1 bool
		private INTEGER NOT NULL DEFAULT 0,
		to_read INTEGER NOT NULL DEFAULT 0,

		-- unix timestamp in seconds
		created_at INT NOT NULL,
		updated_at INT NOT NULL,
		deleted_at INT NOT NULL DEFAULT 0,
		archived_at INTEGER NOT NULL DEFAULT 0
	);

	CREATE INDEX IF NOT EXISTS
	created_at_idx ON bookmarks ( created_at );

	CREATE INDEX IF NOT EXISTS
	deleted_at_idx ON bookmarks ( deleted_at );

	CREATE VIRTUAL TABLE IF NOT EXISTS
	search USING fts5(
		url, title, description, tags, archive_data,
		tokenize = porter,
		content = 'bookmarks',
		content_rowid = 'rowid');
`)

	if err != nil {
		return errors.WithMessage(err, "error creating database")
	}

	n, _ := res.RowsAffected()
	if n > 0 {
		a.Cfg.Logger.Printf("created bookmarks table")
	}
	db.Close()

	err = a.importNetscape()
	if err != nil {
		return errors.WithMessage(err, "error importing Netscape bookmarks")
	}

	go a.startArchiver()

	err = a.startHTTPServer()
	if err == http.ErrServerClosed {
		return nil
	}
	return err
}

func (a *App) importNetscape() error {
	if a.Cfg.ImportNetscapePath == "" {
		return nil
	}
	f, err := os.Open(a.Cfg.ImportNetscapePath)
	if err != nil {
		return err
	}
	defer f.Close()

	doc, err := html.Parse(f)
	if err != nil {
		return err
	}

	db, err := a.db()
	if err != nil {
		return err
	}
	defer db.Close()

	stmt, err := db.Prepare(`
	INSERT INTO bookmarks (url, title, description, tags, private, to_read, created_at)
	VALUES (?, ?, ?, ?, ?, ?, ?)
	`)
	if err != nil {
		return err
	}

	for node := doc.FirstChild.NextSibling.FirstChild.NextSibling.FirstChild.NextSibling.NextSibling.FirstChild; node != nil; node = node.NextSibling {
		bm := Bookmark{}
		if node.Type == html.ElementNode && node.Data == "dd" {
			continue
		}
		if node.Type == html.ElementNode && node.Data == "dt" {
			dtTag := node
			aTag := node.FirstChild
			for _, attr := range aTag.Attr {
				switch attr.Key {
				case "href":
					bm.URL = attr.Val
				case "add_date":
					var err error
					bm.CreatedAt, err = strconv.ParseInt(attr.Val, 10, 64)
					if err != nil {
						return err
					}
				case "private":
					bm.Private = attr.Val == "1"
				case "toread":
					bm.ToRead = attr.Val == "1"
				case "tags":
					bm.Tags = attr.Val
				}
			}
			if aTag.FirstChild != nil {
				bm.Title = aTag.FirstChild.Data
			}
			if dtTag.NextSibling != nil && dtTag.NextSibling.Data == "dd" {
				bm.Description = dtTag.NextSibling.FirstChild.Data
			}

			_, err := stmt.Exec(bm.URL, bm.Title, bm.Description, bm.Tags, bm.Private, bm.ToRead, bm.CreatedAt)
			if err != nil {
				return err
			}
			a.Cfg.Logger.Printf("imported %s - %s", bm.URL, bm.Title)
			continue
		}

		a.Cfg.Logger.Printf("node type: %v, data: %v", node.Type, node.Data)
	}

	return nil
}

func (a *App) Stop() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	return a.deps.server.Shutdown(ctx)
}

func (a *App) startHTTPServer() error {
	router := a.initHTTPRoutes()
	a.deps.server = &http.Server{
		Handler: router,
		Addr:    a.Cfg.ListenAddr,
	}

	a.Cfg.Logger.Printf("listening on http://%s", a.Cfg.ListenAddr)
	return a.deps.server.ListenAndServe()
}
