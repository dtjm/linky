package linky

import (
	"bytes"
	"compress/gzip"
	"database/sql"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"strings"

	"github.com/go-shiori/go-readability"
	"github.com/pkg/errors"
)

// Bookmark is a bookmark model
type Bookmark struct {
	RowID       int
	URL         string
	Title       string
	Description string
	Tags        string
	Private     bool
	ToRead      bool
	CreatedAt   int64
	UpdatedAt   int64

	ArchiveData    []byte
	HasArchiveData bool

	// Search results ranking
	Rank float64
}

func getBookmarkByRowID(db *sql.DB, rowID int) (*Bookmark, error) {
	row := db.QueryRow(`
	SELECT rowid, url, title, description, tags, archive_data
	FROM bookmarks
	WHERE rowid = ?`, rowID)

	bm := Bookmark{}
	err := row.Scan(&bm.RowID, &bm.URL, &bm.Title, &bm.Description, &bm.Tags, &bm.ArchiveData)
	return &bm, err
}

func (b *Bookmark) plaintext() ([]byte, error) {
	if len(b.ArchiveData) == 0 {
		return nil, nil
	}

	ct, err := b.archiveContentType()
	if err != nil {
		return nil, errors.WithMessage(err, "Bookmark.plaintext: error detecting content type")
	}

	if strings.Contains(ct, "html") {
		return b.htmlToText()
	}

	if strings.Contains(ct, "pdf") {
		return b.pdfToText()
	}

	if strings.Contains(ct, "text/plain") {
		zr, err := gzip.NewReader(bytes.NewBuffer(b.ArchiveData))
		if err != nil {
			return nil, err
		}

		return ioutil.ReadAll(zr)
	}

	return nil, errors.Errorf("Bookmark.plaintext: unknown content type: %q", ct)

}

func (b *Bookmark) archiveContentType() (string, error) {
	if len(b.ArchiveData) == 0 {
		return "", errors.New("linky.Bookmark: no archive data")
	}

	// decompress first 500 bytes
	input := bytes.NewBuffer(b.ArchiveData)
	zr, err := gzip.NewReader(input)
	if err != nil {
		return "", errors.Wrapf(err, "gzip.NewReader (data: %s)", b.ArchiveData)
	}
	defer zr.Close()

	output := make([]byte, 500)
	_, err = zr.Read(output)
	if err != nil {
		return "", errors.WithMessage(err, "gzip.Reader.Read")
	}

	return http.DetectContentType(output), nil
}

func (b *Bookmark) htmlToText() ([]byte, error) {
	parsedURL, err := url.Parse(b.URL)
	if err != nil {
		return nil, err
	}

	input := bytes.NewBuffer(b.ArchiveData)
	zr, err := gzip.NewReader(input)
	if err != nil {
		return nil, errors.WithMessage(err, "htmlToText")
	}
	defer zr.Close()

	article, err := readability.FromReader(zr, parsedURL)
	if err != nil {
		return nil, errors.WithMessage(err, "htmlToText")
	}

	return []byte(article.Content), nil

}

// renders the ArchiveData as text if it's a PDF
func (b *Bookmark) pdfToText() ([]byte, error) {
	zr, err := gzip.NewReader(bytes.NewBuffer(b.ArchiveData))
	if err != nil {
		return nil, err
	}
	defer zr.Close()

	temp, err := ioutil.TempFile("", "")
	if err != nil {
		return nil, err
	}
	defer os.Remove(temp.Name())

	_, err = io.Copy(temp, zr)
	if err != nil {
		return nil, err
	}

	err = temp.Close()
	if err != nil {
		return nil, err
	}

	out := &bytes.Buffer{}
	stderr := &bytes.Buffer{}
	cmd := exec.Command("pdftotext", temp.Name(), "/dev/stdout")
	cmd.Stdout = out
	cmd.Stderr = stderr
	err = cmd.Run()
	if err != nil {
		return nil, errors.Wrapf(err, "error in pdftotext: %s", stderr.String())
	}

	return out.Bytes(), err
}
