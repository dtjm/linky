package linky

import (
	"fmt"
	"html/template"
	"net/url"
	"time"
)

const (
	// HeaderHTML is printed on the header of every page
	HeaderHTML = `<!DOCTYPE html>
<html lang="en">
<head>
	<title>Linky</title>
	<style>
	span.domain {
		font-variant: small-caps;
		font-family: sans-serif; }
	span.tag {
		font-variant: small-caps;
		font-weight: bold;
	}
	h3 .rank {
		font-weight: normal;
		font-size: 14px;
	}
	h3.title { margin-bottom: 0; }
	h3 form { display: inline; }
	a.edit_link {
		margin-left: 1em;
		font-size: 14px;
		font-weight: normal;
	}
	a.archive_link, a.plaintext_link {
		font-size: 14px;
		font-weight: normal;
	}
	div.description {
		font-family: monospace;
		white-space: pre-wrap;
		width: 50%;
	}
	form table {
		width: 100%;
	}
	form th {
		padding-right: 10px;
		width: 1%;
	}
	form td input,
	form td textarea {
		width: 100%;
	}
	form textarea {
		height: 150px;
		font-family: monospace;
	}
	</style>
</head>
<body>
`

	// FooterHTML is printed at the end of every page
	FooterHTML = `
</body>
`
	titleBarHTML = `
		<style>
		#title_bar {
			color: #eee;
			padding: 10px;
			position: fixed;
			top: 0;
			width: 100%;
		}
		</style>
		<div id=title_bar>
			<a href="javascript:q=location.href;if(document.getSelection)%7Bd=document.getSelection();%7Delse%7Bd='';%7D;p=document.title;void(open('http://127.0.0.1:7171/add?url='+encodeURIComponent(q)+'&description='+encodeURIComponent(d)+'&title='+encodeURIComponent(p),'Linky','toolbar=no,width=700,height=400'));">
				bookmarklet
			</a>
		</div>
`
)

var (
	funcs = map[string]interface{}{
		"urlToDomain": func(urlStr string) (string, error) {
			u, err := url.Parse(urlStr)
			if err != nil {
				return "", err
			}

			return u.Host, nil
		},
		"elapsedTime": func(ts int64) string {
			return time.Now().Sub(time.Unix(ts, 0)).String()
		},
		"fmtRank": func(r float64) string {
			return fmt.Sprintf("%.2f", -r)
		},
	}

	// BookmarkTemplate is a template for a bookmark
	BookmarkTemplate = template.Must(template.New("bookmark").Funcs(funcs).Parse(`
	<div class=bookmark>
	<h3 class=title>
			{{ if .Rank }}<span class=rank>{{ .Rank | fmtRank }}</span>{{ end }}
			<a href="{{ .URL }}">{{ .Title }}</a>
			<a class=edit_link href="/add?url={{ .URL }}">edit</a>
			{{ if .HasArchiveData }}
			<a class=archive_link href="/archive?url={{ .URL }}">archive</a>
			<a class=plaintext_link href="/plaintext?rowid={{ .RowID }}">plaintext</a>
			{{ end }}

			<form method=POST action="/archive">
				<input type=hidden name=rowid value="{{ .RowID }}" />
				<button>Re-archive</button>
			</form>

			<form method=POST action="/delete">
				<input type=hidden name=url value="{{ .URL }}" />
				<button>delete</button>
			</form>
		</h3>
		<div class=metadata>
			<span class=domain>{{ .URL | urlToDomain }}</span>
			{{ .Tags }}
		</div>
		<div class=description>
			{{- .Description -}}
		</div>
	</div>
`))

	// AddTemplate is a template for a adding or editing a bookmark
	AddTemplate = template.Must(template.New("add").Funcs(funcs).Parse(`
	{{ if .CreatedAt }}
	<div class=created_at>created {{ .CreatedAt | elapsedTime }} ago, updated {{ .UpdatedAt | elapsedTime }} ago</div>
	{{ end }}
	<form method=POST>
		<input type=hidden name=rowid value={{ .RowID }} />
		<table>
			<tr>
				<th><label for=url>url</label></td>
				<td><input type=text name=url id=url value="{{ .URL }}"></td>
			</tr>
			<tr>
				<th><label for=title>title</label></td>
				<td><input type=text name=title id=title value="{{ .Title }}"/></td>
			</tr>
			<tr>
				<th><label for=description>description</label></td>
				<td><textarea name=description id=description>{{- .Description -}}</textarea></td>
			</tr>
			<tr>
				<th><label for=tags>tags</label></td>
				<td><input type=text name=tags id=tags value="{{ .Tags }}" /></td>
			</tr>
			<tr>
				<th><label for=to_read>to_read</label></td>
				<td><input type=checkbox name=to_read id=to_read value="1" {{ if .ToRead }}checked{{ end }}/></td>
			</tr>
			<tr>
				<td></td>
				<td><input type=submit value="Save" /></td>
			</tr>
		</table>
	</form>
`))
)
