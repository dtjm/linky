package linky

import (
	"bytes"
	"compress/gzip"
	"database/sql"
	"io/ioutil"
	"net/http"
	"sync/atomic"
	"time"
)

func (a *App) clearArchive(rowID int) error {
	// TODO: do this in a transaction
	// TODO: Get the bookmark contents so we can delete from the search index

	db, err := a.db()
	if err != nil {
		return err
	}
	defer db.Close()

	// Remove the archive state on the row
	_, err = db.Exec(`
	UPDATE bookmarks
	SET archive_data = '', archived_at = 0, archive_status = 0
	WHERE rowid = ?`, rowID)
	return err
}

func (a *App) startArchiver() {
	n := atomic.AddInt64(&a.state.archiverSema, 1)
	defer atomic.AddInt64(&a.state.archiverSema, -1)
	if n > 1 {
		return
	}

	a.Cfg.Logger.Printf("archiver: starting loop")
	defer a.Cfg.Logger.Printf("archiver: exiting loop")

	db, err := a.db()
	if err != nil {
		a.Cfg.Logger.Printf("error opening database: %s", err)
		return
	}
	defer db.Close()

	stmt, err := db.Prepare(`
	SELECT rowid, url, title, description, tags
	FROM bookmarks
	WHERE archived_at = 0
	ORDER BY rowid DESC
	LIMIT 1
	`)
	if err != nil {
		a.Cfg.Logger.Fatalf("error preparing archive SELECT: %s", err)
	}
	defer stmt.Close()

	client := http.Client{
		Timeout: 10 * time.Second,
	}
	zipBuf := bytes.NewBuffer(nil)
	zw, err := gzip.NewWriterLevel(zipBuf, gzip.BestCompression)
	if err != nil {
		a.Cfg.Logger.Fatalf("unable to create gzip writer: %s", err)
	}

	for {
		row := stmt.QueryRow()
		bm := Bookmark{}

		markAsArchived := func(status int, html []byte) error {
			zipBuf.Reset()
			zw.Reset(zipBuf)

			// strip BOM and whitespace
			if len(html) > 3 && string(html[:3]) == "\xEF\xBB\xBF" {
				html = html[3:]
			}
			html = bytes.TrimSpace(html)

			if len(html) > 0 {
				_, err := zw.Write(html)
				if err != nil {
					a.Cfg.Logger.Printf("archiver(%d): error compressing HTML: %s", bm.RowID, err)
					return err
				}
				err = zw.Close()
				if err != nil {
					a.Cfg.Logger.Printf("archiver(%d): error closing zlib writer: %s", bm.RowID, err)
					return err
				}

				bm.ArchiveData = zipBuf.Bytes()
				a.addToSearchIndex(&bm)
			}

			res, err := db.Exec(`UPDATE bookmarks
		SET archive_data = ?, archived_at = ?, archive_status = ?
		WHERE rowid = ?`, zipBuf.String(), time.Now().Unix(), status, bm.RowID)
			if err != nil {
				a.Cfg.Logger.Printf("archiver(%d): error updating row: %s", bm.RowID, err)
				return err
			}

			if n, err := res.RowsAffected(); err != nil {
				a.Cfg.Logger.Printf("archiver(%d): updated %d rows", bm.RowID, n)
				return err
			}
			return nil
		}

		err := row.Scan(&bm.RowID, &bm.URL, &bm.Title, &bm.Description, &bm.Tags)
		if err == sql.ErrNoRows {
			return
		}
		a.Cfg.Logger.Printf("archiver(%d): fetching %s", bm.RowID, bm.URL)

		if err != nil {
			markAsArchived(500, nil)
			a.Cfg.Logger.Printf("archiver(%d): error scanning row: %s", bm.RowID, err)
			continue
		}

		resp, err := client.Get(bm.URL)
		if err != nil {
			markAsArchived(500, nil)
			a.Cfg.Logger.Printf("archiver(%d): error in GET: %s", bm.RowID, err)
			continue
		}

		if resp.StatusCode >= 300 {
			markAsArchived(resp.StatusCode, nil)
			a.Cfg.Logger.Printf("archiver(%d): received non-2xx code: %s", bm.RowID, resp.Status)
			continue
		}

		content, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			markAsArchived(600, nil)
			a.Cfg.Logger.Printf("archiver(%d): error in body read: %s", bm.RowID, err)
			continue
		}

		markAsArchived(resp.StatusCode, content)
		a.Cfg.Logger.Printf("archiver(%d): complete", bm.RowID)
	}
}
