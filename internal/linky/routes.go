package linky

import (
	"bytes"
	"compress/gzip"
	"compress/zlib"
	"database/sql"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/RadhiFadlillah/go-readability"
	"github.com/julienschmidt/httprouter"
)

func (a *App) initHTTPRoutes() http.Handler {
	router := httprouter.New()

	router.HandlerFunc("GET", "/", a.routeGetIndex)
	router.HandlerFunc("GET", "/add", a.routeGetAdd)

	// GET /archive renders the archived content
	router.HandlerFunc("GET", "/archive", a.routeGetArchive)

	// POST /archive re-archives the bookmark
	router.HandlerFunc("POST", "/archive", a.routePostArchive)

	router.HandlerFunc("POST", "/add", a.routePostAdd)
	router.HandlerFunc("POST", "/delete", a.routePostDelete)

	router.HandlerFunc("GET", "/plaintext", a.routeGetPlaintext)

	return router
}

func (a *App) routeGetIndex(rw http.ResponseWriter, req *http.Request) {
	searchQuery := req.URL.Query().Get("search")

	rw.Header().Set("Content-type", "text/html; charset=utf-8")

	_, err := io.WriteString(rw, HeaderHTML)
	if err != nil {
		return
	}

	fmt.Fprintf(rw, `
	<div>
		<div id=search>
			<form method=GET action="/">
				<input type=text name=search value="%s"/>
				<input type=submit value=Search />
			</form>
		</div>
	</div>
	`, searchQuery)

	limit, err := strconv.Atoi(req.URL.Query().Get("limit"))
	if err != nil {
		limit = 100
	}

	db, err := a.db()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error opening database: %s", err)
		return
	}
	defer db.Close()

	var rows *sql.Rows
	if searchQuery != "" {
		rows, err = db.Query(`
		SELECT rowid, url, title, description, tags, (archive_data != '') AS has_archive_data, rank
		FROM search
		WHERE search MATCH ?
		ORDER BY rank
		LIMIT ?
		`, searchQuery, limit)
	} else {
		rows, err = db.Query(`
		SELECT rowid, url, title, description, tags, (archive_data != '') AS has_archive_data, 0
		FROM bookmarks
		ORDER BY created_at DESC
		LIMIT ?`, limit)
	}
	if err != nil {
		fmt.Fprintf(rw, "error querying database: %s", err)
		return
	}

	err = renderBookmarks(rw, rows)
	if err != nil {
		fmt.Fprintf(rw, "%s", err)
	}

	io.WriteString(rw, FooterHTML)
}

func renderBookmarks(w io.Writer, rows *sql.Rows) error {

	bm := Bookmark{}
	for rows.Next() {
		err := rows.Scan(&bm.RowID, &bm.URL, &bm.Title, &bm.Description,
			&bm.Tags, &bm.HasArchiveData, &bm.Rank)
		if err != nil {
			return fmt.Errorf("error scanning row: %s", err)
		}

		if bm.Title == "" {
			bm.Title = bm.URL
		}

		err = BookmarkTemplate.Execute(w, &bm)
		if err != nil {
			return fmt.Errorf("error executing template %q: %s", BookmarkTemplate.Name(), err)
		}
	}

	return nil
}

func (a *App) routePostAdd(rw http.ResponseWriter, req *http.Request) {
	now := time.Now().Unix()

	rowID := strings.TrimSpace(req.PostFormValue("rowid"))

	url := strings.TrimSpace(req.PostFormValue("url"))
	desc := strings.TrimSpace(req.PostFormValue("description"))
	tags := strings.TrimSpace(req.PostFormValue("tags"))
	title := strings.TrimSpace(req.PostFormValue("title"))
	toRead := strings.TrimSpace(req.PostFormValue("to_read")) == "1"
	defer a.startArchiver()

	db, err := a.db()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error opening database: %s", err)
		return
	}
	defer db.Close()

	if rowID == "0" {
		_, err := db.Exec(`
		INSERT INTO bookmarks (url, title, description, tags, to_read, created_at, updated_at)
		VALUES (?, ?, ?, ?, ?, ?, ?)
		`, url, title, desc, tags, toRead, now, now)

		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(rw, "error inserting: %s", err)
			return
		}

		io.WriteString(rw, `<script>close()</script>`)
		return
	}

	_, err = db.Exec(`
		UPDATE bookmarks
		SET url = ?, title = ?, description = ?, tags = ?, to_read = ?, updated_at = ?
		WHERE rowid = ?
	`, url, title, desc, tags, toRead, now, rowID)

	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error updating: %s", err)
		return
	}

	http.Redirect(rw, req, "/", http.StatusFound)
}

func (a *App) routeGetAdd(rw http.ResponseWriter, req *http.Request) {

	bm := Bookmark{}
	urlStr := req.URL.Query().Get("url")

	u, err := url.Parse(urlStr)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(rw, "invalid URL: %s", err)
		return
	}

	rw.Header().Set("Content-type", "text/html; charset=utf-8")

	db, err := a.db()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error opening database: %s", err)
		return
	}
	defer db.Close()

	if urlStr != "" {
		row := db.QueryRow(`
				SELECT rowid, url, title, description, tags, to_read, created_at, updated_at
				FROM bookmarks
				WHERE url = ?`, urlStr)
		err = row.Scan(&bm.RowID, &bm.URL, &bm.Title, &bm.Description, &bm.Tags,
			&bm.ToRead, &bm.CreatedAt, &bm.UpdatedAt)

		if err == sql.ErrNoRows {
			bm.URL = req.URL.Query().Get("url")
			bm.Title = strings.TrimSpace(req.URL.Query().Get("title"))

			bm.Description = strings.TrimSpace(req.URL.Query().Get("description"))

			if bm.Description == "" {
				article, err := readability.FromURL(u, 10*time.Second)
				if err != nil {
					a.Cfg.Logger.Printf("unable to parse readability info: %s", err)
					bm.Description = req.URL.Query().Get("description")
				} else {
					bm.Description = fmt.Sprintf("%s\n\n%s", article.Meta.Excerpt, article.Meta.Author)
				}
				bm.Description = strings.TrimSpace(bm.Description)
			}
		} else if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(rw, "error scanning row: %s", err)
			return
		}
	}

	_, err = io.WriteString(rw, HeaderHTML)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Display insert/update form
	err = AddTemplate.Execute(rw, &bm)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error executing template %q: %s", AddTemplate.Name(), err)
		return
	}
}

func (a *App) routePostDelete(rw http.ResponseWriter, req *http.Request) {
	url := req.PostFormValue("url")

	db, err := a.db()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error opening database: %s", err)
		return
	}
	defer db.Close()

	res, err := db.Exec(`
	UPDATE bookmarks
	SET deleted_at = ?
	WHERE url = ?`, time.Now().Unix(), url)

	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error deleting %q: %s", url, err)
		return
	}

	n, _ := res.RowsAffected()
	a.Cfg.Logger.Printf("[POST /delete?url=%s] row=%d", url, n)

	http.Redirect(rw, req, "/", http.StatusFound)
}

func (a *App) routeGetArchive(rw http.ResponseWriter, req *http.Request) {
	urlStr := req.URL.Query().Get("url")
	filter := req.URL.Query().Get("filter")

	db, err := a.db()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error opening database: %s", err)
		return
	}
	defer db.Close()

	row := db.QueryRow(`
		SELECT archive_data FROM bookmarks
		WHERE url = ?`, urlStr)
	var data []byte
	err = row.Scan(&data)
	if err != nil {
		fmt.Fprintf(rw, "error reading from db: %s", err)
		return
	}

	// If client accepts gzip, then just send what we have
	if strings.Contains(req.Header.Get("Accept-Encoding"), "gzip") {
		// Detect content type by checking first 500 bytes of data
		head := make([]byte, 500)
		// reading from zr will yield uncompressed HTML
		zr, err := gzip.NewReader(bytes.NewBuffer(data))
		if err != nil {
			fmt.Fprintf(rw, "error creating gzip reader for content type detection: %s", err)
			return
		}
		defer zr.Close()
		_, err = zr.Read(head)
		if err != nil {
			fmt.Fprintf(rw, "error reading head of content: %s", err)
			return
		}

		ct := http.DetectContentType(head)
		rw.Header().Set("Content-Type", ct)
		rw.Header().Set("Content-Encoding", "gzip")

		rw.Write(data)
		return
	}

	// If client doesn't accept gzip, we will have to unzip ourselves
	buf := bytes.NewBuffer(data)
	// reading from zr will yield uncompressed HTML
	zr, err := zlib.NewReader(buf)
	if err != nil {
		fmt.Fprintf(rw, "error creating zlib reader: %s", err)
	}
	defer zr.Close()

	if filter == "" {
		_, err = io.Copy(rw, zr)
		if err != nil {
			fmt.Fprintf(rw, "error copying from zlib reader: %s", err)
		}
		return
	}

	if filter == "readability" {
		u, err := url.Parse(urlStr)
		if err != nil {
			fmt.Fprintf(rw, "error parsing URL: %s", err)
			return
		}

		article, err := readability.FromReader(zr, u)
		if err != nil {
			fmt.Fprintf(rw, "error parsing HTML with readability: %s", err)
			return
		}

		io.WriteString(rw, article.RawContent)
		return
	}
}

func (a *App) routePostArchive(rw http.ResponseWriter, req *http.Request) {
	rowID := req.PostFormValue("rowid")

	i, err := strconv.Atoi(rowID)
	if err != nil {
		fmt.Fprintf(rw, "unable to parse rowid: %s", err)
		return
	}

	err = a.clearArchive(i)
	if err != nil {
		fmt.Fprintf(rw, err.Error())
		return
	}

	go a.startArchiver()

	http.Redirect(rw, req, "/", http.StatusFound)
}

func (a *App) routeGetPlaintext(rw http.ResponseWriter, req *http.Request) {
	rowid := req.URL.Query().Get("rowid")
	r, err := strconv.Atoi(rowid)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(rw, "invalid rowid query arg: %s", err)
		return
	}

	db, err := a.db()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error opening database: %s", err)
		return
	}
	defer db.Close()

	bm, err := getBookmarkByRowID(db, r)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error getting bookmark: %s", err)
		return
	}

	text, err := bm.plaintext()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(rw, "error rendering plaintext: %s", err)
		return
	}

	rw.Write(text)
}
