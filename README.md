**linky** is a bookmarks manager which was written as a replacement for
Pinboard which no longer seems to archive or perform full-text search.  
It's even more anti-social
than Pinboard as it stores your data in a local sqlite database.

```
Usage of linky:
  -data-dir string
    	path to sqlite database (default "/Users/snguyen/.linky.sqlite3")
  -import-netscape string
    	path to file containing Netscape bookmarks
  -listen-addr string
    	host:port to listen for HTTP requests (default "127.0.0.1:7171")
```

Use the HTML export format from Pinboard to import into **linky**:
https://pinboard.in/export/

## Feature roadmap

- [x] Bookmarklet
- [x] Search
- [x] Archiving & full text search for HTML
- [x] Archiving & full text search for PDFs
- [ ] Archive resources from the domain of the page (images & css)
- [ ] Publish to S3
- [ ] Render description as Markdown
