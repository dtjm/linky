FROM golang:1-alpine

EXPOSE 7171
WORKDIR /go/src/gitlab.com/dtjm/linky

# CGO build dependencies
RUN apk add gcc libc-dev

ENV CGO_CFLAGS=-DSQLITE_ENABLE_FTS5 \
    LINKY_FLAGS='-listen-addr=:7171'

ADD . /go/src/gitlab.com/dtjm/linky

RUN go build -v -o /usr/local/bin/linky main.go

CMD [ "bash", "-c", "exec /usr/local/bin/linky ${LINKY_FLAGS}" ]
