package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/dtjm/linky/internal/linky"
)

func main() {
	var err error
	a := linky.App{}
	a.Cfg.Logger = log.New(os.Stderr, "[linky] ", log.LstdFlags|log.Lshortfile)

	flag.StringVar(&a.Cfg.DBPath, "data-dir",
		filepath.Join(os.Getenv("HOME"), ".linky.sqlite3"),
		"path to sqlite database")
	flag.StringVar(&a.Cfg.ListenAddr, "listen-addr", "127.0.0.1:7171",
		"host:port to listen for HTTP requests")
	flag.StringVar(&a.Cfg.ImportNetscapePath, "import-netscape", "", "path to file containing Netscape bookmarks")
	flag.Parse()

	go func() {
		err := a.Start()
		if err != nil {
			a.Cfg.Logger.Fatalf("error in app.Start: %s", err)
		}
	}()

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, syscall.SIGTERM, syscall.SIGINT)

	sig := <-sigChan
	a.Cfg.Logger.Printf("received %q signal", sig.String())
	err = a.Stop()
	if err != nil {
		a.Cfg.Logger.Fatalf("error in app.Stop: %s", err)
	}
}
